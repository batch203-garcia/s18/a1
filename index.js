function activityOneSum(num1, num2) {
    let sum = num1 + num2;
    console.log("Displayed sum of " + num1 + " and " + num2);
    console.log(sum);

}

activityOneSum(5, 15);

function activityOneDiff(num1, num2) {

    let diff = num1 - num2;
    console.log("Displayed difference of " + num1 + " and " + num2);
    console.log(diff);

}

activityOneDiff(20, 5);

function activityTwoProd(num1, num2) {

    let prod = num1 * num2;
    console.log("Displayed product of " + num1 + " and " + num2)
    return prod;

}

let product = activityTwoProd(50, 10);
console.log(product);

function activityTwoQuo(num1, num2) {

    let quo = num1 / num2;
    console.log("Displayed quotient of " + num1 + " and " + num2)
    return quo;

}

let quotient = activityTwoQuo(50, 10);
console.log(quotient);

function activityThreeCircle(radius) {
    let circleArea = Math.PI * Math.pow(radius, 2);
    console.log("The result of getting the area of circle with " + radius + " radius: ");
    return circleArea;
}

let areaOfCircle = activityThreeCircle(15);
console.log(areaOfCircle);

function activityFour(num1, num2, num3, num4) {

    let sum = (num1 + num2 + num3 + num4);
    let average = sum / 4;
    console.log("The average of " + num1 + "," + num2 + "," + num3 + " and " + num4 + ":");
    return average;

}

let average = activityFour(20, 40, 60, 80);
console.log(average);

function activityFive(num1, num2) {

    let score = num1 / num2;
    let isPassingScore = score !== 0;
    console.log("Is " + num1 + "/" + num2 + " a passing score?")
    return isPassingScore;

}

let scoreInputs = activityFive(38, 50);
console.log(scoreInputs);